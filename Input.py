from pynput import mouse, keyboard

class Input():
    def __init__(self):
        listener = keyboard.Listener(
            on_press=self.onKeyPress,
            on_release=self.onKeyRelease)
        listener.start()
        self.Key = keyboard.Key
        self.KeyCode = keyboard.KeyCode
        self.keyboardCtrl = keyboard.Controller()

    def onKeyPress(self, key):
        if key not in self.input:
            self.input.append(key);
        pass

    def onKeyRelease(self, key):
        if(key in self.input):
            self.input.remove(key)
        pass
