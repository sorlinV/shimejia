import sys
#import sh_window
import os
import yaml
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class Animation():
    def __init__(self, name, keys):
        self.name = name
        self.keys = keys
        self.mirror = False
    def onAnimationEnd(self):
        pass

class Animator():
    animations = []
    currentAnimation = None
    currentKey = 0
    def __init__(self, pathfiles):
        if not hasattr(self, 'renderer') or not hasattr(self, 'resize') or not hasattr(self, 'show'):
            print("[Error] Impossible de d'ajouté un animator a cette element")
            return
        self.pathfiles = pathfiles
        self.timeranim = QTimer(self)
        self.timeranim.timeout.connect(self.updateAnimation)
        self.timeranim.start(400)

    def addAnimation(self, name, keys):
        self.animations.append(Animation(name, keys))
        pass

    def playAnimation(self, animationName):
        for anim in self.animations:
            if animationName == anim.name:
                self.currentAnimation = anim
        pass

    def updateAnimation(self):
        if self.currentAnimation != None:
            self.currentKey += 1
            if self.currentKey >= len(self.currentAnimation.keys):
                self.currentKey = 0
            idKey = self.currentAnimation.keys[self.currentKey]
            pixmap = QPixmap(self.pathfiles.format(id=idKey))
            if self.currentAnimation.mirror:
                pixmap = pixmap.transformed(QTransform().scale(-1, 1))
                pass
            self.renderer.setPixmap(pixmap)
            self.resize(pixmap.size())
            self.show()

    def destroys(self):
        self.timeranim.stop();
