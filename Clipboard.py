from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from Object import Object
import sys, inspect
import pyperclip, collections
from functools import partial

class Clipboard():
    def __init__(self):
        self.oldclips = collections.deque(maxlen=10)
        self.timerclip = QTimer(self)
        self.timerclip.timeout.connect(self.updateclip)
        self.timerclip.start(10)

    def pasteClip(self, oldclip):
        pyperclip.copy(oldclip)
        self.keyboardCtrl.press(self.Key.backspace)
        self.keyboardCtrl.release(self.Key.backspace)
        with self.keyboardCtrl.pressed(self.Key.ctrl):
            self.keyboardCtrl.press('v')
            self.keyboardCtrl.release('v')

    def genClipMenu(self):
        actionMenu = QMenu()
        for oldclip in reversed(self.oldclips):
            actionMenu.addAction(oldclip[0:30], partial(self.pasteClip, oldclip))
            pass
        actionMenu.exec_(QCursor().pos())

    def onClipperChange(self, value):
        self.oldclips.append(value)
        pass

    def updateclip(self):
        if len(self.oldclips) <= 0 or self.oldclips[-1] != pyperclip.paste():
            self.onClipperChange(pyperclip.paste())
        if self.Key.cmd in self.input and self.KeyCode.from_char('v') in self.input:
            self.genClipMenu()
            self.input=[]
            pass
