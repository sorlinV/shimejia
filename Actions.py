import math
from Event import Event
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import random
from datetime import datetime

class Climb(Event):
    def __init__(self, shimeji):
        Event.__init__(self, "climb", shimeji, shimeji, shimeji)

    def startFct(self, shimeji):
        shimeji.playAnimation("climb")
        pass
    def playFct(self, shimeji):
        x = QCursor().pos().x() - (shimeji.size().width() / 2)
        y = QCursor().pos().y() - (shimeji.size().height() / 2)
        shimeji.moveTo(QVector2D(x, y))
        distance = math.sqrt(math.pow(shimeji.x() - x, 2) + math.pow(shimeji.y() - y, 2))
        if distance < 10:
            self.stop()
    def stopFct(self, shimeji):
        shimeji.playAnimation("idle")
        shimeji.translate(QVector2D(-shimeji.x(), -shimeji.y()))
        pass

class Blink(Event):
    def __init__(self, shimeji):
        Event.__init__(self, "blink", shimeji, shimeji, shimeji)

    def startFct(self, shimeji):
        shimeji.playAnimation("blink")
        pass
    def playFct(self, shimeji):
        x = QCursor().pos().x() - (shimeji.size().width() / 2)
        y = QCursor().pos().y() - (shimeji.size().height() / 2)
        shimeji.moveTo(QVector2D(x, y))
        distance = math.sqrt(math.pow(shimeji.x() - x, 2) + math.pow(shimeji.y() - y, 2))
        if distance < 10:
            self.stop()
    def stopFct(self, shimeji):
        shimeji.playAnimation("idle")
        shimeji.translate(QVector2D(-shimeji.x(), -shimeji.y()))
        pass

class Dividing(Event):
    def __init__(self, shimeji):
        Event.__init__(self, "dividing", shimeji, shimeji, shimeji)

    def startFct(self, shimeji):
        shimeji.playAnimation("dividing")
        pass
    def playFct(self, shimeji):
        x = QCursor().pos().x() - (shimeji.size().width() / 2)
        y = QCursor().pos().y() - (shimeji.size().height() / 2)
        shimeji.moveTo(QVector2D(x, y))
        distance = math.sqrt(math.pow(shimeji.x() - x, 2) + math.pow(shimeji.y() - y, 2))
        if distance < 10:
            self.stop()
    def stopFct(self, shimeji):
        shimeji.playAnimation("idle")
        shimeji.translate(QVector2D(-shimeji.x(), -shimeji.y()))
        pass

class Eat(Event):
    def __init__(self, shimeji):
        Event.__init__(self, "eat", shimeji, shimeji, shimeji)

    def startFct(self, shimeji):
        self.duration = random.randrange(2, 5)
        self.startTime = datetime.timestamp(datetime.now())
        shimeji.playAnimation("eat")
        pass
    def playFct(self, shimeji):
        if datetime.timestamp(datetime.now()) > self.startTime + self.duration:
            self.stop()
        # x = QCursor().pos().x() - (shimeji.size().width() / 2)
        # y = QCursor().pos().y() - (shimeji.size().height() / 2)
        # shimeji.moveTo(QVector2D(x, y))
        # distance = math.sqrt(math.pow(shimeji.x() - x, 2) + math.pow(shimeji.y() - y, 2))
        # if distance < 10:
        #     self.stop()
        pass
    def stopFct(self, shimeji):
        shimeji.playAnimation("idle")
        # shimeji.translate(QVector2D(-shimeji.x(), -shimeji.y()))
        pass

class Fall(Event):
    def __init__(self, shimeji):
        Event.__init__(self, "fall", shimeji, shimeji, shimeji)

    def startFct(self, shimeji):
        shimeji.playAnimation("fall")
        pass
    def playFct(self, shimeji):
        shimeji.moveTo(QVector2D(shimeji.pos.x(), shimeji.pos.y() + 20))
        pass
    def stopFct(self, shimeji):
        shimeji.playAnimation("idle")
        pass

class Gather(Event):
    def __init__(self, shimeji):
        Event.__init__(self, "gather", shimeji, shimeji, shimeji)

    def startFct(self, shimeji):
        shimeji.playAnimation("gather")
        pass
    def playFct(self, shimeji):
        x = QCursor().pos().x() - (shimeji.size().width() / 2)
        y = QCursor().pos().y() - (shimeji.size().height() / 2)
        shimeji.moveTo(QVector2D(x, y))
        distance = math.sqrt(math.pow(shimeji.x() - x, 2) + math.pow(shimeji.y() - y, 2))
        if distance < 10:
            self.stop()
    def stopFct(self, shimeji):
        shimeji.playAnimation("idle")
        shimeji.translate(QVector2D(-shimeji.x(), -shimeji.y()))
        pass

class Hidde(Event):
    def __init__(self, shimeji):
        Event.__init__(self, "hidde", shimeji, shimeji, shimeji)

    def startFct(self, shimeji):
        shimeji.playAnimation("hidde")
        pass
    def playFct(self, shimeji):
        x = QCursor().pos().x() - (shimeji.size().width() / 2)
        y = QCursor().pos().y() - (shimeji.size().height() / 2)
        shimeji.moveTo(QVector2D(x, y))
        distance = math.sqrt(math.pow(shimeji.x() - x, 2) + math.pow(shimeji.y() - y, 2))
        if distance < 10:
            self.stop()
    def stopFct(self, shimeji):
        shimeji.playAnimation("idle")
        shimeji.translate(QVector2D(-shimeji.x(), -shimeji.y()))
        pass

class Jump(Event):
    def __init__(self, shimeji):
        Event.__init__(self, "jump", shimeji, shimeji, shimeji)

    def startFct(self, shimeji):
        shimeji.playAnimation("jump")
        pass
    def playFct(self, shimeji):
        x = QCursor().pos().x() - (shimeji.size().width() / 2)
        y = QCursor().pos().y() - (shimeji.size().height() / 2)
        shimeji.moveTo(QVector2D(x, y))
        distance = math.sqrt(math.pow(shimeji.x() - x, 2) + math.pow(shimeji.y() - y, 2))
        if distance < 10:
            self.stop()
    def stopFct(self, shimeji):
        shimeji.playAnimation("idle")
        shimeji.translate(QVector2D(-shimeji.x(), -shimeji.y()))
        pass

class Walk(Event):
    def __init__(self, shimeji):
        Event.__init__(self, "Walk", shimeji, shimeji, shimeji)

    def startFct(self, shimeji):
        shimeji.playAnimation("walk")
        pass
    def playFct(self, shimeji):
        x = QCursor().pos().x() - (shimeji.size().width() / 2)
        y = QCursor().pos().y() - (shimeji.size().height() / 2)
        shimeji.moveTo(QVector2D(x, y))
        distance = math.sqrt(math.pow(shimeji.x() - x, 2) + math.pow(shimeji.y() - y, 2))
        if distance < 10:
            self.stop()
    def stopFct(self, shimeji):
        shimeji.playAnimation("idle")
        shimeji.translate(QVector2D(-shimeji.x(), -shimeji.y()))
        pass

class WormWalk(Event):
    def __init__(self, shimeji):
        Event.__init__(self, "worm-walk", shimeji, shimeji, shimeji)

    def startFct(self, shimeji):
        self.duration = random.randrange(2, 5)
        self.startTime = datetime.timestamp(datetime.now())
        self.direction = (random.random() * 2) - 1
        shimeji.playAnimation("worm-walk")
        if self.direction > 0:
            shimeji.currentAnimation.mirror = True
        if self.direction < 0:
            shimeji.currentAnimation.mirror = False
        pass
    def playFct(self, shimeji):
        shimeji.moveTo(shimeji.pos + QVector2D(self.direction, 0))
        if datetime.timestamp(datetime.now()) > self.startTime + self.duration:
            self.stop()
            pass
        pass
    def stopFct(self, shimeji):
        shimeji.playAnimation("idle")
        pass

class Idle(Event):
    def __init__(self, shimeji):
        Event.__init__(self, "idle", shimeji, shimeji, shimeji)

    def startFct(self, shimeji):
        shimeji.playAnimation("idle")
        pass
    def playFct(self, shimeji):
        pass
    def stopFct(self, shimeji):
        shimeji.playAnimation("idle")
        #shimeji.translate(QVector2D(-shimeji.x(), -shimeji.y()))
        pass


class Talk(Event):
    def __init__(self, shimeji):
        Event.__init__(self, "idle", shimeji, shimeji, shimeji)

    def startFct(self, shimeji):
        shimeji.playAnimation("idle")
        pass
    def playFct(self, shimeji):
        pass
    def stopFct(self, shimeji):
        shimeji.playAnimation("idle")
        #shimeji.translate(QVector2D(-shimeji.x(), -shimeji.y()))
        pass

class Grab(Event):
    def __init__(self, shimeji):
        Event.__init__(self, "grab", shimeji, shimeji, shimeji)

    def startFct(self, shimeji):
        pass
    def playFct(self, shimeji):
        x = QCursor().pos().x() - (shimeji.size().width() / 2)
        y = QCursor().pos().y() - (shimeji.size().height() / 2)
        shimeji.translateTo(QVector2D(x, y))
        pass
    def stopFct(self, shimeji):
        shimeji.playAnimation("idle")
        pass
