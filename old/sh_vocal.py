from gtts import gTTS
from config2.config import config
import speech_recognition as sr
import tempfile
import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'
from pygame import mixer
import time

def playaudiofile(_fd):
    mixer.init()
    _fd.seek(0)
    mixer.music.load(_fd)
    mixer.music.play(0)
    while mixer.music.get_busy():
        time.sleep(0.1);

def listen():
    message = ''
    r = sr.Recognizer()
    with sr.Microphone() as source:
        audio = r.listen(source)
        try:
            message = r.recognize_google(audio, language=config.lang)
        except sr.RequestError:
            return(message)
        except sr.UnknownValueError:
            return(message)
    return(message)

def talk(_text):
    tts = gTTS(text=_text, lang=config.lang)
    f = tempfile.TemporaryFile()
    tts.write_to_fp(f)
    playaudiofile(f)
    f.close()