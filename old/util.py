import math
import threading, time


def set_interval(func, sec):
    def func_wrapper():
        set_interval(func, sec)
        func()

    t = threading.Timer(sec, func_wrapper)
    t.start()
    t.stop = t.cancel
    return t


class setInterval:
    def __init__(self, _action, _interval):
        self.interval = _interval
        self.action = _action
        self.stopEvent = threading.Event()
        thread = threading.Thread(target=self.__setInterval)
        thread.start()

    def __setInterval(self):
        nextTime = time.time()+self.interval
        while not self.stopEvent.wait(nextTime-time.time()):
            nextTime += self.interval
            self.action()

    def stop(self):
        self.stopEvent.set()


class Vector:
    x = 0
    y = 0

    def __init__(self, _x, _y):
        self.x = _x
        self.y = _y

    def __add__(self, other):
        return Vector(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Vector(self.x - other.x, self.y - other.y)

    def set_lenght(self, _lenght):
        currentLenght = math.sqrt(self.x ** 2 + self.y ** 2)
        if currentLenght > _lenght:
            self.x = (_lenght / currentLenght) * self.x
            self.y = (_lenght / currentLenght) * self.y

    def set_lenght_x(self, _lenghtx):
        self.y = (_lenghtx / self.x) * self.y
        self.x = _lenghtx

    def set_lenght_y(self, _lenghty):
        self.x = (_lenghty / self.y) * self.x
        self.y = _lenghty


def getFileContent(pathAndFileName):
    with open(pathAndFileName, 'r') as theFile:
        # Return a list of lines (strings)
        # data = theFile.read().split('\n')

        # Return as string without line breaks
        # data = theFile.read().replace('\n', '')

        # Return as string
        data = theFile.read()
        return data
