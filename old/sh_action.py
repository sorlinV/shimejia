

class Command():
    def __init__(self, _name, _action, **_kwargs):
        self.name = _name
        self.action = _action
        self.kwargs = _kwargs

    def play(self, **_kwargs):
        self.action(_kwargs)
