from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from util import *

class Bulle(QWidget):
    def __init__(self, text, _shimeji):
        QWidget.__init__(self)
        self.shimeji = _shimeji
        text = list(text)
        j = 0
        for i in range(0, len(text)):
            j += 1
            if j > 50 and text[i] == ' ':
                text[i] = '\n'
                j = 0
        text = "".join(text)
        self.size_x = min(len(text) + 10, 60) * 6
        self.size_y = (text.count('\n')+1) * 10 + 80
        self.total_delay = ((text.count(' ')+1) / 2.5) * 2000
        self.setWindowFlags(self.windowFlags() | Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.resize(self.size_x, self.size_y)

        # SET bulle image
        self.image = QLabel(self)
        self.image.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.image.setAlignment(Qt.AlignCenter)
        self.image.resize(self.size_x, self.size_y)
        self.pixmap = QPixmap('bulle.png')
        self.pixmap = self.pixmap.scaled(self.size_x, self.size_y, Qt.IgnoreAspectRatio)
        self.image.setPixmap(self.pixmap)

        # SET bulle text
        self.text_ui = QLabel(text, self)
        self.text_ui.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.text_ui.setAlignment(Qt.AlignCenter)
        self.text_ui.resize(self.text_ui.size().height(), self.text_ui.size().width() - 50)
        self.text_ui.move(self.text_ui.pos().x(), self.text_ui.pos().y() - 100)

        self.layout = QGridLayout()
        self.layout.addWidget(self.text_ui)

        self.setLayout(self.layout)

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update)
        self.timer.start(10)
        self.show()

    def update(self):
        self.total_delay -= 10
        if self.total_delay < 0:
            self.stop()
        pos = self.shimeji.transform.position + Vector(100, -100)
        self.move(QPoint(pos.x, pos.y))

    def stop(self):
        self.close()
        self.timer.stop()
