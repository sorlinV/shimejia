import sys
import os
import math
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import vocal
import time

class Bulle(QWidget):
    def __init__(self, text):
        text = list(text)
        j = 0
        for i in range(0, len(text)):
            j += 1
            if j > 50 and text[i] == ' ':
                text[i] = '\n'
                j = 0
        text = "".join(text)
        self.size_x = min(len(text) + 10, 60) * 6
        self.size_y = (text.count('\n')+1) * 10 + 80
        self.total_delay = (text.count(' ')+1) / 2.5 * 1000
        QWidget.__init__(self)
        self.setWindowFlags(self.windowFlags() | Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.resize(self.size_x, self.size_y)
        
        # SET bulle image
        self.image = QLabel(self)
        self.image.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.image.setAlignment(Qt.AlignCenter)
        self.image.resize(self.size_x, self.size_y)
        self.pixmap = QPixmap('bulle.png')
        self.pixmap = self.pixmap.scaled(self.size_x, self.size_y, Qt.IgnoreAspectRatio)
        self.image.setPixmap(self.pixmap)
        
        # SET bulle text
        self.text_ui = QLabel(text, self)
        self.text_ui.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.text_ui.setAlignment(Qt.AlignCenter)
        self.text_ui.resize(self.text_ui.size().height(), self.text_ui.size().width() - 50)
        self.text_ui.move(self.text_ui.pos().x(), self.text_ui.pos().y() - 100)
        

        self.layout = QGridLayout()
        self.layout.addWidget(self.text_ui, 0, 0)

        self.setLayout(self.layout)
        
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update)
        self.timer.start(10)
        self.show()
      
    def update(self):
        self.total_delay -= 10
        if (self.total_delay < 0):
            self.close()
            self.timer.stop()
        self.move(self.parent.pos() + QPoint(100, -100))

class Shimejia(QWidget):
    dir = QPoint(0, 0)
    old_pos = QPoint(0, 0)
    
    def __init__(self):
        QWidget.__init__(self)
        self.children = []
        self.bulles = []
        self.setWindowTitle('Shimejiwa')
        self.shimPressed = False
        self.setWindowFlags(self.windowFlags() | Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.label = QLabel(self)
        pixmap = QPixmap('./img/shime1.png')
        self.label.setPixmap(pixmap)
        self.resize(pixmap.size())
        self.anim = 1
        self.asMouseEvent = False
        timer = QTimer(self)
        timer.timeout.connect(self.update)
        timer.start(10)
        
        timer_anim = QTimer(self)
        timer_anim.timeout.connect(self.update_animation)
        timer_anim.start(500)
        self.show()
    
    def mouseMoveEvent(self, event):
        if event.buttons() == Qt.LeftButton:
            self.asMouseEvent = True
            x = QCursor().pos().x() - (self.size().width() / 2)
            y = QCursor().pos().y() - (self.size().height() / 2)
            pos = QPoint(x, y)
            self.dir = (pos - self.old_pos) * 2
            self.old_pos = pos
            self.move(pos)

    def killChildren(self):
        for child in self.children:
            child.close()
    
    def mousePressEvent(self, event):
        if event.buttons() == Qt.RightButton:
            menu = QMenu()
            clear_action = menu.addAction("talk", lambda: vocal.talk("shasha tu est le plus fort"))
            clear_action = menu.addAction("listen", lambda: self.text(vocal.listen()))
            clear_action = menu.addAction("TP", lambda: self.move(QPoint(0, 0)))
            clear_action = menu.addAction("Text", self.text)
            clear_action = menu.addAction("Create child", self.create)
            clear_action = menu.addAction("Suicide", lambda: self.close())
            clear_action = menu.addAction("KillParent", lambda: self.parent.close())
            clear_action = menu.addAction("KillChildren", self.killChildren)
            action = menu.exec_(QCursor().pos()) 

    def text(self, _text="Je parle"):
        bulle = Bulle(_text)
        bulle.show()
        bulle.parent = self
        self.bulles.append(bulle)

    def create(self):
        child = Shimejia()
        child.show()
        child.parent = self
        self.children.append(child)
    
    def mouseReleaseEvent(self, event):
        self.asMouseEvent = False

    def setAnimation(self, _debut, _fin, _mirror = False):
        self.anim += 1
        if self.anim > _fin:
            self.anim = _debut
        pixmap = QPixmap('./img/shime' + str(self.anim) + '.png')
        if _mirror:
            pixmap = pixmap.transformed(QTransform().scale(-1, 1))
            self.label.setPixmap(pixmap)
        else:
            self.label.setPixmap(pixmap)
    
    def goRight(self):
        if self.dir.x() <= 0:
            return False
        if self.dir.x() > 0:
            return True
    
    def update_animation(self):
        if not self.asMouseEvent:
            if self.y >= QDesktopWidget().screenGeometry().height() - self.size().height():
                self.setAnimation(2, 3, self.goRight())
            elif self.y < QDesktopWidget().screenGeometry().height() - self.size().height():
                self.setAnimation(4, 4, self.goRight())
            else:
                self.setAnimation(1, 1, self.goRight())
        if self.asMouseEvent:
            if self.dir.x() == 0:
                self.setAnimation(1, 1)
            elif math.fabs(self.dir.x()) < 5:
                if self.goRight():
                    self.setAnimation(5, 5)
                elif not self.goRight():
                    self.setAnimation(6, 6)
            elif math.fabs(self.dir.x()) > 5 and math.fabs(self.dir.x()) < 10:
                if self.goRight():
                    self.setAnimation(7, 7)
                elif not self.goRight():
                    self.setAnimation(8, 8)
            else:
                if self.goRight():
                    self.setAnimation(9, 9)
                elif not self.goRight():
                    self.setAnimation(10, 10)
    
    def update(self):
        if self.dir.x() != 0:
            self.dir.setX(self.dir.x() + math.copysign(1, -self.dir.x()))
        if self.dir.y() != 50:
            self.dir.setY(self.dir.y() + math.copysign(1, 50 - self.dir.y()))
        self.x = self.pos().x() + self.dir.x() / 10
        self.y = min(self.pos().y() + self.dir.y() / 10, QDesktopWidget().screenGeometry().height() - self.size().height())
        if not self.asMouseEvent:
            self.move(self.x, self.y)
                

app = QApplication.instance() 
if not app:
    app = QApplication(sys.argv)
    
fen = Shimejia()
fen.parent = fen #le personnage se suicide si il n'as pas de parent c'est triste


app.exec_()