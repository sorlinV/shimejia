from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from util import *
"""
class NewActionWindow(QWidget):
    def __init__(self, _shimejiAction):
        QWidget.__init__(self)
        self.shimejiAction = _shimejiAction
        self.title = 'Shimeji Nouvelle Action'
        self.width = 400
        self.height = 400
        self.setWindowTitle(self.title)

        # Create textbox
        self.textbox = QLineEdit(self)
        self.textbox.move(20, 20)
        self.textbox.resize(280,40)

        # Create a button in the window
        self.button = QPushButton('Show text', self)
        self.button.move(20,80)

        # connect button to function on_click
        self.button.clicked.connect(self.on_click)
        self.show()

    def on_click(self):
        textboxValue = self.textbox.text()
        QMessageBox.question(self, 'Message - pythonspot.com', "You typed: " + textboxValue, QMessageBox.Ok, QMessageBox.Ok)
        self.textbox.setText("")
        #self.shimejiAction.AddAction()

"""
class NewActionWindow(QWidget):
    def __init__(self, text, _shimeji):
        QWidget.__init__(self)
        self.shimeji = _shimeji
        print("New action window")
        text = list(text)
        j = 0
        for i in range(0, len(text)):
            j += 1
            if j > 50 and text[i] == ' ':
                text[i] = '\n'
                j = 0
        text = "".join(text)
        self.size_x = min(len(text) + 10, 60) * 6
        self.size_y = (text.count('\n')+1) * 10 + 80
        self.total_delay = ((text.count(' ')+1) / 2.5) * 2000
        self.resize(self.size_x, self.size_y)

        # SET bulle image
        self.image = QLabel(self)
        self.image.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.image.setAlignment(Qt.AlignCenter)
        self.image.resize(self.size_x, self.size_y)
        self.pixmap = QPixmap('bulle.png')
        self.pixmap = self.pixmap.scaled(self.size_x, self.size_y, Qt.IgnoreAspectRatio)
        self.image.setPixmap(self.pixmap)

        # SET bulle text
        self.text_ui = QLabel(text, self)
        self.text_ui.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.text_ui.setAlignment(Qt.AlignCenter)
        self.text_ui.resize(self.text_ui.size().height(), self.text_ui.size().width() - 50)
        self.text_ui.move(self.text_ui.pos().x(), self.text_ui.pos().y() - 100)

        self.layout = QGridLayout()
        self.layout.addWidget(self.text_ui)

        self.setLayout(self.layout)

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update)
        self.timer.start(10)
        self.show()

    def update(self):
        self.total_delay -= 10
        if self.total_delay < 0:
            self.stop()
        self.move(QPoint(50, 50))

    def stop(self):
        self.close()
        self.timer.stop()