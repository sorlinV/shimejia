import socket
from config2.config import config
from util import set_interval

#tips https://github.com/317070/python-twitch-stream/blob/master/twitchstream/chat.py


class MessageTwitch:
    def __init__(self, _input):
        self.user = _input.split('#')[1].split(' ')[0]
        self.text = _input.split('#')[1].split(':')[1]

    def __eq__(self, other):
        if other is None:
            return False
        return self.text == other.text and self.user == other.user


class TwitchChat:
    all_messages = []

    def __init__(self):
        server = 'irc.chat.twitch.tv'
        port = 6667
        nickname = config.twitch.name
        token = config.twitch.oauth
        channel = '#isidoris'
        self.interval = None
        self.serveur = socket.socket()
        self.serveur.connect((server, port))
        self.serveur.send(bytes('PASS ' + token + '\r\n', 'utf-8'))
        self.serveur.send(bytes('NICK ' + nickname + '\r\n', 'utf-8'))
        self.serveur.send(bytes('JOIN ' + channel + '\r\n', 'utf-8'))
        self.start()

    def start(self):
        i = 10
        self.interval = set_interval(self.get_messsage, 1)

    def stop(self):
        if self.interval is not None:
            self.interval.stop()

    def get_messsage(self):
        message = None
        input = self.serveur.recv(2048).decode('utf-8')
        if "PRIVMSG" in input:
            message = MessageTwitch(input)
            self.all_messages.append(message)

    def message(self, _msg):
        self.serveur.send(bytes("PRIVMSG #{} :{}\r\n".format(config.twitch.name, _msg), 'utf-8'))

    @property
    def last_message(self):
        size = len(self.all_messages)
        if size == 0:
            return None
        return self.all_messages[size - 1]

