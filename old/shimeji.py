from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import sys
import time
from util import *
import sh_vocal
import sh_window
import sh_bulle
import sh_twitch
import sh_newaction

class Visual(QWidget):
    def __init__(self, _shimeji):
        QWidget.__init__(self)
        self.shimeji = _shimeji
        self.setWindowTitle('Shimejia')
        self.setWindowFlags(self.windowFlags() | Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.image = QLabel(self)
        pixmap = QPixmap('./img/shime1.png')
        self.image.setPixmap(pixmap)
        self.resize(pixmap.size())
        self.shimeji.transform.size = Vector(pixmap.size().height(), pixmap.size().width())
        self.timer_anim = QTimer(self)
        self.timer_anim.timeout.connect(self.update_visual)
        self.timer_anim.start(20)
        self.show()
        
    def update_visual(self):
        x = round(self.shimeji.transform.position.x)
        y = round(self.shimeji.transform.position.y)
        if x != 0 and y != 0:
            self.move(QPoint(x, y))
    
    def mouseMoveEvent(self, event):
        self.shimeji.mouseMoveEvent(event)
    
    def mouseReleaseEvent(self, event):
        self.shimeji.mouseReleaseEvent(event)

    def stop(self):
        self.timer_anim.stop()
        self.close()

class Transform:
    position = Vector(100, 100)
    direction = Vector(0, 0)
    size = Vector(0, 0)
    speed = 4

    def __init__(self, _shimeji):
        self.shimeji = _shimeji

    def center(self):
        return Vector(self.position.x - self.size.x / 2, self.position.y - self.size.y / 2)

    def move(self, _x, _y):
        new_position = Vector(_x - self.size.x / 2, _y - self.size.y / 2)
        direction = self.position - new_position
        self.position = new_position
        
    def moveTo(self, _x, _y):
        new_position = Vector(_x - self.size.x / 2, _y - self.size.y / 2)
        self.direction = self.position - new_position
        self.direction.set_lenght(self.speed)
        i = 0
        for window in sh_window.get_all_windows():
            if self.close_vertical(window):
                print(window.title, ' est close vertical du shimeji')
            if self.close_horizontal(window):
                print(window.title, ' est close horizontal du shimeji')
        self.position -= self.direction
        
    def moveAdd(self, _x, _y):
        new_position = self.position
        new_position += Vector(_x, _y)
        new_position.y = min(new_position.y, 915)
        self.direction = self.position - new_position
        self.direction.set_lenght(self.speed)
        i = 0
        for window in sh_window.get_all_windows():
            if self.close_vertical(window):
                print(window.title, ' est close vertical du shimeji')
            if self.close_horizontal(window):
                print(window.title, ' est close horizontal du shimeji')
        self.position -= self.direction
        
    def close_vertical(self, _window):
        """
        if _window.is_maximized != True:
            rect = _window.rect
            left = rect.x - 120
            right = rect.x + rect.w - 120
            top = rect.y - 120
            bottom = rect.y + rect.h - 120
            if (abs(left - self.center().x) < 5 or abs(right - self.center().x) < 5) and top < self.center().y and bottom > self.center().y:
                return _window
        """
        return None

    def close_horizontal(self, _window):
        """
        if _window.is_maximized != True:
            left = _window.left - 120
            right = _window.right - 120
            top = _window.top - 120
            bottom = _window.bottom - 120
            if (abs(top - self.center().y) < 5 or abs(bottom - self.center().y) < 5) and left < self.center().x and right > self.center().x:
                return _window
        """
        return None

class ShimejiAction():
    def __init__(self):
        self.actionMenu = QMenu()
        self.actionMenu.addAction("create new Action", lambda: sh_newaction.NewActionWindow("toto", self))
        self.actionMenu.addAction("Quit", lambda: self.stop())

    def rightClickAction(self):
        self.actionMenu.exec_(QCursor().pos())

    def stop(self):
        return

class Shimeji(ShimejiAction):
    def __init__(self):
        ShimejiAction.__init__(self)
        self.asMouseEvent = False
        self.transform = Transform(self)
        self.visual = Visual(self)
        self.children = []
        self.bulle = None
        self.chat = sh_twitch.TwitchChat()
        self.message = None
        self.timer = QTimer(self.visual)
        self.timer.timeout.connect(self.update)
        self.timer.start(10)

    def update(self):
        if not self.asMouseEvent:
            self.transform.moveAdd(0, 10)
        if self.chat.last_message is not None and self.message != self.chat.last_message:
            self.message = self.chat.last_message
            self.talk(self.message.text)

    def mouseMoveEvent(self, event):
        if event.buttons() == Qt.LeftButton:
            self.asMouseEvent = True
            x = QCursor().pos().x()
            y = QCursor().pos().y()
            self.transform.move(x, y)
        if event.buttons() == Qt.RightButton:
            self.rightClickAction()
    
    def mouseReleaseEvent(self, event):
        self.asMouseEvent = False

    def talk(self, _text):
        self.bulle = sh_bulle.Bulle(_text, self)
        time.sleep(1)
        sh_vocal.talk(_text)

    def stop(self):
        if self.bulle is not None: self.bulle.stop()
        self.timer.stop()
        self.visual.stop()
        self.chat.stop()


app = QApplication.instance()
if not app:
    app = QApplication(sys.argv)

shimeji = Shimeji()

sys.exit(app.exec_())