import sys

if sys.platform == "linux":
    from ewmh import EWMH
    ewmh = EWMH()

    class RECT():
        def __init__(self, x, y, w, h):
            self.x = x
            self.y = y
            self.w = w
            self.h = h
        def toString(self):
            return (str(self.x)+":"+str(self.y)+", "+str(self.w)+":"+str(self.h))

    class Window():
        def __init__(self, client):
            self.before = client
            while client.query_tree().parent != ewmh.root:
                client = client.query_tree().parent
            self.client = client

        def __eq__(self, other):
            return self.client == other.client

        def maximize(self):
            windll.user32.ShowWindow(self.hwnd, SW_MAXIMIZE)

        def minimize(self):
            windll.user32.ShowWindow(self.hwnd, SW_MINIMIZE)

        def close(self):
            ewmh.setCloseWindow(self.client)

        @property
        def title(self):
            return(ewmh.getWmName(self.before).decode())

        @property
        def is_focus(self):
            return(False)

        @property
        def is_minimized(self):
            return(False)

        @property
        def is_maximized(self):
            return(False)

        @property
        def rect(self):
            lin_rect = self.client.get_geometry()
            return(RECT(lin_rect.x, lin_rect.y, lin_rect.width, lin_rect.height))

        @property
        def depth(self):
            return(self.client.get_geometry().depth)

        @property
        def segments(self):
            segments = []
            segments.append({'xa': self.rect.x, 'ya': self.rect.y, 'xb': self.rect.x + self.rect.w, 'yb': self.rect.y})
            segments.append({'xa': self.rect.x, 'ya': self.rect.y, 'xb': self.rect.x, 'yb': self.rect.y + self.rect.h})
            segments.append({'xa': self.rect.x + self.rect.w, 'ya': self.rect.y, 'xb': self.rect.x + self.rect.w, 'yb': self.rect.y + self.rect.h})
            segments.append({'xa': self.rect.x, 'ya': self.rect.y + self.rect.h, 'xb': self.rect.x + self.rect.w, 'yb': self.rect.y + self.rect.h})
            return segments


    def get_all_windows():
        windows = []

        for client in ewmh.getClientList():
            windows.append(Window(client))
        return windows

else:
    from ctypes import *
    from ctypes import wintypes

    EnumWindows = windll.user32.EnumWindows
    EnumWindowsProc = WINFUNCTYPE(c_bool, c_int, POINTER(c_int))

    SW_MINIMIZE = 6
    SW_MAXIMIZE = 3
    WM_CLOSE = 0x0010

    class RECT(Structure):
        _fields_ = [
            ('left',   wintypes.LONG ),
            ('top',    wintypes.LONG ),
            ('right',  wintypes.LONG ),
            ('bottom', wintypes.LONG )
        ]

        @property
        def x(self):
            return self.left
        @property
        def y(self):
            return self.top
        @property
        def w(self):
            return self.right-self.left
        @property
        def h(self):
            return self.bottom-self.top
        def toString(self):
            return (str(self.x)+":"+str(self.y)+", "+str(self.w)+":"+str(self.h))

    class Window():
        depth = 0
        def __init__(self, _hwnd):
            self.hwnd = _hwnd

        def __eq__(self, other):
            return self.hwnd == other.hwnd

        def maximize(self):
            windll.user32.ShowWindow(self.hwnd, SW_MAXIMIZE)

        def minimize(self):
            windll.user32.ShowWindow(self.hwnd, SW_MINIMIZE)

        def close(self):
            windll.user32.PostMessageA(self.hwnd, WM_CLOSE , 0, 0)
            return result != 0

        @property
        def title(self):
            length = windll.user32.GetWindowTextLengthW(self.hwnd)
            buff = create_unicode_buffer(length + 1)
            windll.user32.GetWindowTextW(self.hwnd, buff, length + 1)
            return(buff.value)

        @property
        def is_focus(self):
            hWnd = windll.user32.GetForegroundWindow()
            if hWnd == 0:
                return None
            return Window(hWnd) == self

        @property
        def is_minimized(self):
            return windll.user32.IsIconic(self.hwnd) != 0

        @property
        def is_maximized(self):
            return not self.is_minimized

        @property
        def rect(self):
            rect = RECT()
            windll.user32.GetWindowRect(self.hwnd, byref(rect))
            return(rect)

        @property
        def segments(self):
            segments = []
            segments.append({'xa': self.rect.x, 'ya': self.rect.y, 'xb': self.rect.x + self.rect.w, 'yb': self.rect.y})
            segments.append({'xa': self.rect.x, 'ya': self.rect.y, 'xb': self.rect.x, 'yb': self.rect.y + self.rect.h})
            segments.append({'xa': self.rect.x + self.rect.w, 'ya': self.rect.y, 'xb': self.rect.x + self.rect.w, 'yb': self.rect.y + self.rect.h})
            segments.append({'xa': self.rect.x, 'ya': self.rect.y + self.rect.h, 'xb': self.rect.x + self.rect.w, 'yb': self.rect.y + self.rect.h})
            return segments

    def get_all_windows():
        windows = []

        def foreach_window(hwnd, lParam):
            if windll.user32.IsWindowVisible(hwnd):
                win = Window(hwnd)
                if win.title != "" and win.title not in ["Program Manager", "Microsoft Store", "Calculatrice", "Paramètres"]:
                    windows.append(win)
            return True
        EnumWindows(EnumWindowsProc(foreach_window), 0)
        return windows
