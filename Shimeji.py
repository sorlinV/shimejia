import os
import yaml
import sh_window
import Utils
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from Animator import Animator
from Actions import *
from Event import Event
from Object import Object
from random import randrange

class Shimeji(Animator, Object):
    actions = []
    currentAction = None
    iAction = 0
    def __init__(self, path):
        self.loadConfig(path)
        Object.__init__(self)
        Animator.__init__(self, os.path.join(path, self.config['images-name']))
        for anim in self.config['animations']:
            self.addAnimation(anim, self.config['animations'][anim])
            pass
        self.addAction()
        self.playAction("fall")

    def loadConfig(self, path):
        with open(os.path.join(path, "config.yml")) as file:
            self.config = yaml.load(file, Loader=yaml.FullLoader)

    def update(self):
        if Qt.LeftButton in self.input and self.currentAction.name != "grab":
            self.playAction("grab")
        elif self.onFloor() and not Qt.LeftButton in self.input:
            if self.currentAction.isEnd or self.currentAction.name == "fall" or self.currentAction.name == "grap":
                value = randrange(2)
                print(value)
                if value == 0:
                    self.playAction("worm-walk")
                else:
                    self.playAction("eat")
                pass
        elif not Qt.LeftButton in self.input:
            self.playAction("fall")
        self.currentAction.play()
        return

    def playAction(self, actionName):
        for action in self.actions:
            if action.name == actionName:
                if self.currentAction and not self.currentAction.isEnd:
                    self.currentAction.stop()
                if self.currentAction and self.currentAction.isEnd:
                    self.currentAction.isEnd = False
                self.currentAction = action
                self.currentAction.start()
            pass
        pass

    def addAction(self):
        self.actions.append(Idle(self))
        self.actions.append(Blink(self))
        self.actions.append(Climb(self))
        self.actions.append(Dividing(self))
        self.actions.append(Eat(self))
        self.actions.append(Fall(self))
        self.actions.append(Gather(self))
        self.actions.append(Hidde(self))
        self.actions.append(Jump(self))
        self.actions.append(Walk(self))
        self.actions.append(WormWalk(self))
        self.actions.append(Grab(self))
        pass

    def onFloor(self):
        for win in sh_window.get_all_windows():
            for seg in win.segments:
                if Utils.intersect(QPoint(seg['xa'], seg['ya']), QPoint(seg['xb'], seg['yb']), self.pos + QVector2D(0, self.height()), self.pos + QVector2D(0, self.height() - 20)):
                    self.moveTo(QVector2D(self.pos.x(), seg['ya'] - self.height() + 2))
                    return True
                pass
            pass
        return False

    def onWallLeft(self):
        for win in sh_window.get_all_windows():
            for seg in win.segments:
                if Utils.intersect(QPoint(seg['xa'], seg['ya']), QPoint(seg['xb'], seg['yb']), self.pos, self.pos + QVector2D(-10, 0)):
                    return True
                pass
            pass
        return False

    def onWallRight(self):
        for win in sh_window.get_all_windows():
            for seg in win.segments:
                if Utils.intersect(QPoint(seg['xa'], seg['ya']), QPoint(seg['xb'], seg['yb']), self.pos + QVector2D(0, self.width()), self.pos + QVector2D(0, self.width() + 10)):
                    return True
                pass
            pass
        return False
