from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from Input import Input

class Object(QWidget, Input):
    def __init__(self):
        Input.__init__(self)
        self.renderer = None
        self.speed = 3
        self.input = []
        self.childrenTransform = []
        self.pos = QVector2D(0, 0)
        #Init visuel window
        QWidget.__init__(self)
        self.setWindowTitle('Shimejiwa')
        self.setWindowFlags(self.windowFlags() | Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.renderer = QLabel(self)

        #Init update
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update)
        self.timer.start(10)

    def moveTo(self, objectif, ignoreChild = False):
        allSegmentsWindows = []
        vec = objectif - self.pos
        if vec.length() > self.speed:
            vec.normalize()
            self.translate(vec * self.speed, ignoreChild)
        else:
            self.translate(objectif - self.pos, ignoreChild)
        return

    def translateTo(self, pos, ignoreChild = False):
        if not ignoreChild:
            for child in self.childrenTransform:
                posChild = child.pos + (self.pos - child.pos)
                child.translateTo(posChild)
        self.pos = pos
        self.move(int(self.pos.x()), int(self.pos.y()))

    def addChildren(self, obj):
        self.childrenTransform.append(obj)

    def translate(self, vec: QVector2D, ignoreChild = False):
        self.translateTo(self.pos + vec, ignoreChild);

    def update(self):
        self.show()
        return

    def mousePressEvent(self, event):
        if event.button() not in self.input:
            self.input.append(event.button());
        pass

    def mouseReleaseEvent(self, event):
        if(event.button() in self.input):
            self.input.remove(event.button())
        pass

    def keyPressEvent(self, event):
        if event.key() not in self.input:
            self.input.append(event.key());
        pass

    def keyReleaseEvent(self, event):
        if(event.key() in self.input):
            self.input.remove(event.key())
        pass

    def setInterval(self, callback, duration=0):
        timeoutTimer = QTimer()
        timeoutTimer.setSingleShot(True)
        timeoutTimer.timeout.connect(self.destroy)
        timeoutTimer.start(duration)

    def debug(self):
        self.setAttribute(Qt.WA_TranslucentBackground, False)

    def destroy(self):
        self.timer.stop()
        self.close()
