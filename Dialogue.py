from Object import Object
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class Dialogue(Object):
    def __init__(self, text):
        Object.__init__(self)
        self.text_ui = QLabel(text, self)

        font = self.text_ui.font()
        fm = QFontMetrics(font)
        pixelsWide = fm.width(text)
        pixelsHigh = fm.height()
        self.text_ui.setStyleSheet("QLabel { background-color : white; color : black; }")
        self.resize(pixelsWide, pixelsHigh)
        wordNb = text.count(' ')
        self.timerEnd = QTimer(self)
        self.timerEnd.timeout.connect(self.destroy)
        self.timerEnd.start(wordNb * 1800)

    def destroy(self):
        Object.destroy(self)
        self.timerEnd.stop()
