
class Event():
    isEnd = False
    def __init__(self, name, argStart, argPlay, argStop):
        self.name = name;
        self.argStart = argStart;
        self.argPlay = argPlay;
        self.argStop = argStop;
    def start(self):
        self.startFct(self.argStart)
    def play(self):
        self.playFct(self.argPlay)
    def stop(self):
        self.isEnd = True
        self.stopFct(self.argStop)

    def startFct(self, arg):
        pass
    def playFct(self, arg):
        pass
    def stopFct(self, arg):
        pass
